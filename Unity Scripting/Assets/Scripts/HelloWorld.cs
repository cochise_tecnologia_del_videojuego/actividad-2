using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelloWorld : MonoBehaviour
{
    public string message = "Hello World!";
    // Update is called once per frame
    void Update()
    {
        print(message);
    }
}
